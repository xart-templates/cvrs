jQuery.noConflict();
jQuery(document).ready(function($){

	$('html').addClass('js');

	$('[data-toggle="search"]').on('click',function(event){
		event.preventDefault();
		$('body').toggleClass('search-on');
	});

	$('[data-toggle="menu"]').on('click',function(event){
		event.preventDefault();
		$('body').toggleClass('menu-on');
	});

	// file input style
	$('input[type=file]').each(function(){
		var uploadText = $(this).data('value');
		if (uploadText) {
			var uploadText = uploadText;
		} else {
			var uploadText = 'Choose file';
		}
		var inputClass=$(this).attr('class');
		$(this).wrap('<span class="fileinputs"></span>');
		$(this)
			.parents('.fileinputs')
			.append($('<span class="fakefile"/>')
			.append($('<input class="input-file-text" type="text" />')
			.attr({'id':$(this)
			.attr('id')+'__fake','class':$(this).attr('class')+' input-file-text'}))
			.append('<input type="button" value="'+uploadText+'">'));
		$(this)
			.addClass('type-file')
			.css('opacity',0);
		$(this)
			.bind('change mouseout',function(){$('#'+$(this).attr('id')+'__fake')
			.val($(this).val().replace(/^.+\\([^\\]*)$/,'$1'))
		})
	});

	// placeholder
	if (document.createElement('input').placeholder==undefined){
		$('[placeholder]').focus(function(){
			var input=$(this);
			if(input.val()==input.attr('placeholder')){
				input.val('');
				input.removeClass('placeholder')
			}
		}).blur(function(){
			var input=$(this);
			if(input.val()==''||input.val()==input.attr('placeholder')){
				input.addClass('placeholder');
				input.val(input.attr('placeholder'))
			}
		}).blur()
	}

	// iCheck
	$('input[type="checkbox"],input[type="radio"]').iCheck();

	// selectBoxIt
	$('select:not([multiple])').selectBoxIt();

});